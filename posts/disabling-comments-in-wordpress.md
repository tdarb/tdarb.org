---
title: "Disabling Comments in WordPress"
layout: post
date: December 28, 2020
summary: "Disabling WordPress comments across both past and future posts can be a little tricky. Let's make things as simple as possible"
---

*I seem to come across a decent amount of clients* and users online that have a difficult time knowing how to disable comments for both future *and* previous blog posts. It isn’t the easiest for both use cases, so let’s break it down.

## Back to the future

Disabling comments on all *future* blog posts is fairly straightforward:

<ol><li>Navigate to your WordPress admin dashboard</li><li>Go to <strong>Settings</strong> &gt; <strong>Discussion</strong></li><li>Uncheck <strong>Allow people to submit comments on new posts</strong></li><li>That’s it!</li></ol>

## But what about old posts?

Have no fear - “Bulk Actions” are here to save the day! In order to retroactively disable comments on older posts, do the following:

<ol><li>Navigate to <strong>Posts</strong></li><li>Select the specific posts you would like to disable comments on (or select all of them)</li><li>Click the <strong>Bulk Actions</strong> drop-down menu and choose <strong>Edit</strong></li><li>Press <strong>Apply</strong></li><li>In the <strong>Bulk Edit</strong> view, look for the <strong>Comments</strong> drop-down </li><li>Select <strong>Do Not Allow</strong> and click <strong>Update</strong></li><li>That’s it!</li><div></div></ol>

I hope this article saves others any headaches in the future. WordPress itself could certainly make this more streamlined, but for now what we have works. Happy (not) commenting!



