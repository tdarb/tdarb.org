---
title: Using User-Select
date: June 04, 2019
layout: post
description: Set your user text selection to highlight all or none of your content
summary: Set your user text selection to highlight all or none of your content with
  a couple lines of simple CSS.
redirect_from: "/2019/06/04/user-select/"
---

*Highlighting text in order to copy, cut or paste content* is a staple action across the web. Now, what if I told you the ability to control what a user can select is configurable with a single CSS property?

## Introducing the CSS property

Simply put, the `user-select` property is defined as follows:

> `user-select` controls whether the user can select text (cursor or otherwise)

## The CSS

The property's available attributes are very straightforward (just remember to target specific browsers for full support!)

```css
/* Default */
p.default {
    user-select: auto;
    -moz-user-select: auto;
    -webkit-user-select: auto;
}

/* Disable the user from selecting text */
p.no-select {
    user-select: none;
    -moz-user-select: none;
    -webkit-user-select: none;
}

/* Select all text when user clicks */
p.select-all {
    user-select: all;
    -moz-user-select: all;
    -webkit-user-select: all;
}
```

## Let's see it in action

Try selecting the separate paragraph elements in the CodePen below:

<p class="codepen" data-height="393" data-theme-id="0" data-default-tab="result" data-user="bradleytaunt" data-slug-hash="QRooZp" style="height: 393px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="CSS: User Select Property">
  <span>See the Pen <a href="https://codepen.io/bradleytaunt/pen/QRooZp/">
  CSS: User Select Property</a> by Bradley Taunt (<a href="https://codepen.io/bradleytaunt">@bradleytaunt</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## Browser Support

The great news is `user-select` is fully supported across all modern browsers (even as far back as IE10!)

