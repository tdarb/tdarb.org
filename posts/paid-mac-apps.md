---
title: Happily Paying For macOS Apps
date: June 29, 2022
---

It's no secret that I am a huge advocate for open source software. A solid chunk of my day-to-day workload is done so via FOSS[^0] systems. I also manage a handful of fun side projects that are normally shipped under either MIT or GPL licensing. But that doesn't mean I still don't enjoy _some_ non-free, proprietary software.

So, I thought I would share my collection of macOS applications that I happily paid for. (There aren't many since my needs are limited)


### Design Tool: Sketch

My day job requires me to use Figma, which is totally fine but not nearly as polished as Sketch. Yes, Figma is cross-platform. Yes, Figma can run directly in the browser. Yes, Figma is free for most smaller team sizes.

But sorry - Sketch is just better.

Since the team at Bohemian Coding have crafted Sketch specifically for macOS it feels native, runs extremely well and fits in with the rest of the ecosystem. The pricing model is okay in my books too, balancing a fine line between _optional_ yearly subscriptions and one-time purchases. It's a smart move and I much prefer it to a forced subscription plan.

URL: [https://www.sketch.com/](https://www.sketch.com/)


### Password Manager: Secrets

I was originally a subscriber to 1Password but couldn't justify spending $7CDN a month for what it was offering. Also, subscriptions suck. After doing some research I stumbled upon Secrets and noticed some things about it that instantly caught my eye:

- No subscriptions! One-time price forever.
- iCloud Sync across devices
- Browser extension support (although I don't use this personally)
- Developed by a one-man team (support indie devs!)

I purchased both the macOS and iOS versions of Secrets after trying out the free version almost immediately. It's wonderful. The UI is clean and flows well with the rest of the Mac ecosystem to give it a native "Apple" feel. Syncing my laptop and iPhone works seamless via iCloud.

And best of all - no monthly fees.

URL: [https://outercorner.com/secrets-mac/](https://outercorner.com/secrets-mac/)


### Transferring Files: Transmit 5

The folks at Panic make incredible Mac and iOS apps. I see them as one of the best in the industry. In the early days of my web development career, I used to run Coda exclusively. I've since moved on the Sublime Text but I still have fond memories of the old Panic editor.

As for FTP access, I still use Panic's Transmit to this day. Beautiful UI that feels snappy even when transferring massive files across servers. Transmit is also a one-time purchase. Thank goodness. (Have I mentioned that I hate software subscriptions?)

URL: [https://panic.com/transmit/](https://panic.com/transmit/)


### Wrapping Up

I know this list only includes 3 applications, but that's truly all the ones I've spent money on. If in the future I happen to purchase any others I will be sure to update this post accordingly.

Thanks for reading,
-- Brad

[^0]: Free &amp; open source software


