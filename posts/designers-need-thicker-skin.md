---
title: Designers Need Thicker Skin
date: July 10, 2017
---

I'm not normally one to comment or even really care about "drama" within our design industry. Opinions are just that and should just be consumed at face value. But this week I was moderately annoyed with a subset of designers in design-land.

### Critique the critics

Designer/design critic Eli Schiff tweeted his thoughts about the newly released promo video from Framer showcasing their new gradient feature. See the initial tweet below:

<figure>
    <img src="/public/images/eli-schiff-twitter.webp" alt="Eli's Tweet">
    <figcaption>Eli Schiff just telling it how it is.</figcaption>
</figure>

Let me begin by saying my views on this comment: I don't care. I honestly don’t feel strongly one way or the other about them making a video promo for gradients. Could it have just been a simple text tweet? Sure. Does it really matter that they decided to make a video for it? Not at all.

But this isn’t the problem.

Other designers took to Twitter and started attacking Eli, not even as a design critic but as a person. This was ugly to see. What happened to civil discussion and giving the other side a chance to say their piece? Calling to "ban him" from sites such as DesignerNews or suggesting to unfollow him on Twitter is unbelievably childish - in an industry that we tote as "accepting of everyone" no less.

That's all I'm going to say because I don’t want to spend too much time on this foolishness, but honestly designers - get some thicker skin.

Yeesh.


