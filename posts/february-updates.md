---
title: "February 2022 Update"
layout: post
date: February 23, 2022
summary: "A brief update about what has been going on this month."
---

It's been a little quiet around here lately and for good reason: my wife gave birth to our third child last Thursday. Her name is Harmony and she was born in the late afternoon weighing in at 7 pounds 8 ounces.

Besides the lack of sleep, everything has been going very well and our boys are happy to finally have their sister “out” in the real world. She caught a small cold at the beginning but soldiered through it well.

As for personal life / work life, I've taken a couple weeks off until Harmony is in a bit more of a routine to make transitioning back to full time easier. I still have a few articles in the works (like setting up a T60 Thinkpad with Linux Mint and using FreeBSD for the first time on an EOL Chromebook) but those will come in time.

I guess I should get back to being a proper dad (I'm writing this on my phone as she sleeps on me and the boys are napping) but I'll be back here posting stupid things in no time.

See you on the other side!