---
title: 'Text Align: Justify'
date: May 22, 2019
layout: post
description: A quick look at the justify value of the text-align CSS property
summary: You have more than likely used the text-align CSS property in your projects
  - but today we're going to take a look at the "justify" value.
redirect_from: "/2019/05/22/text-align-justify/"
---

*The text-align property is fairly well known in the world of CSS*, even among those just starting out with the language. Values such as `center`, `left` and `right` are used often with this property, but a more forgotten option is `justify`.

## What does justify do?

The MDN web docs define the `justify` value for `text-align` as such:

<blockquote>
    <p>The inline contents are justified. Text should be spaced to line up its left and right edges to the left and right edges of the line box, except for the last line.</p>
    <cite><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-align">MDN web docs</a></cite>
</blockquote>

### See it in action

<p class="codepen" data-height="358" data-theme-id="0" data-default-tab="result" data-user="bradleytaunt" data-slug-hash="vwpmNz" style="height: 358px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="Text Align: Justify">
  <span>See the Pen <a href="https://codepen.io/bradleytaunt/pen/vwpmNz/">
  Text Align: Justify</a> by Bradley Taunt (<a href="https://codepen.io/bradleytaunt">@bradleytaunt</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## When should I use this?

It isn't always appropriate to use `justify` in most instances, although it becomes very useful for long form articles or blog posts. Since it takes a heavy influence from original *print* book layouts, the `justify` value helps improve readability for larger chunks of content.

**Fair warning**: it is best to remove any `justify` values when targeting smaller screen sizes. Mobile devices and/or tablets tend to be small enough to break up the content already. This CSS value is better suited for larger viewports.

## Browser support

The good news is that all major browsers support the `justify` value for the `text-align` CSS property. So have some worry-free fun with it!

