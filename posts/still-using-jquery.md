---
title: Yes, I Still Use jQuery
date: April 15, 2019
layout: post
column: single
description: Explaining why I still use jQuery and how it's still usable in any tech
  stack
summary: It isn't the "latest and greatest" library out there. Some even claim that
  the jQuery is "dead" - but not for <i>me</i>.
redirect_from: "/2019/04/15/still-using-jquery/"
---

*I have seen a handful of condescending comments from front-end developers* since the newest build of jQuery ([3.4.0](http://blog.jquery.com/2019/04/10/jquery-3-4-0-released/)) released a couple of days ago. While I understand not all developers share the same work-style or are using the same tech-stack, dismissive comments towards any *useful* library comes off as entitled or elitist.

- "Why would you use jQuery nowadays?" 
- "People are still developing this library?"
- "Why use jQuery when you can use [insert latest trendy web tech here]".

**I still use jQuery**. Well, I may not use the library for all projects since every project is different - but I certainly don't avoid using it solely because "its jQuery". I've always believed in using the best tools for the job.

### Use what works for you

If you produce better work in a shorter amount of time using one of the latest and greatest technologies (React, Vue.js, Angular, etc.) then you should absolutely do so. If another developer can be just as productive building their projects with jQuery, what does it matter in the grand scheme of things?

My thought-process is this: a large percentage of web projects are done for clients not involved in the day-to-day happenings of the developer world. What they want is a solution to a particular problem. Clients don't care how things are done behind the scenes - so long as it is done efficiently and *properly*.

I tend to follow these principles when working on a project (with shared equal importance):

- fast performance
- accessible
- simple UI
- intuitive UX

As long as all of these items are accomplished, I don't care if the project was a direct export from Microsoft Word straight to the web<sup>1</sup>. If it works great, then it works great.

So use whatever tools make you a happier developer, as long as your projects don't suffer because of them.

<small><sup>1</sup>This would obviously be terrible for development, but its just an extreme example</small>

