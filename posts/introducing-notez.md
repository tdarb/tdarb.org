---
title: "Introducing Notez"
layout: post
date: January 13, 2021
summary: "I created a very basic note taking web app that utilizes the browser's local storage and comes packaged with an export function"
---

*I have always been a fan of simple note taking applications*, since I tend to take a lot of random notes throughout the work day. Sometimes I reach for simple pen and paper, but other times it's nice to stay focused jotting down notes on the same device I'm working on.

Previously, I just created quick `notes.txt` files in my open code editor or fell back on the default OS note apps. These worked perfectly fine but often got in my way (or even worse - lost among everything else).

So I said the hell with it and built <a href="https://getnotez.netlify.app">Notez</a>.

## What makes Notez special?

Nothing, really. It's actually a pretty "stupid" app compared to others on the market. But this one is *mine*. Plus, it was fun to piece it together on a random evening before bed.

For those curious, let's take a look at the feature list:

- free and open source (<a href="https://github.com/bradleytaunt/notez">github.com/bradleytaunt/notez</a>)
- uses `localStorage` to keep changes persistent (helpful for browser crashes, etc)
- includes basic export functionality to download content as a simple `output.txt` file

That's it. Mind blowing stuff, eh?

## Making stuff is fun

My main takeaway when building this tiny, dumb app was to just *enjoy building fun stuff*. It crossed my mind several times how there are already hundreds of note taking apps across the vast Internet. I also thought about how others might look at the code I wrote and go, "Oh my God - why did he do it that way? What an idiot!". But I don't care - I had fun making it.

Hopefully you enjoy using it as well!


