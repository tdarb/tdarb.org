---
title: "Sharing The Things We Use"
layout: post
date: July 24, 2021
column: single
summary: "Why everyone with a personal site should share the tools they use."
---

*I always love stumbling across personal websites* that include some form of a "uses" page. A place where the author lists out all the tools they use on a regular basis - whether it be hardware, software or something else entirely. It allows readers to get a slightly more personal peek into the daily work-life of that author and maybe even provides better context for *how* they work.

Since I realized how much I enjoy reading other people's *uses* pages, I've decided to finally publish my own! My list of hardware and software is fairly boring and predictable for a designer/developer - but sharing is caring! My hope is that even one personal out in the great-wide web can find something useful (pun intended!) or least inspiring about my personal setup.

Fell free to check it out: [The Things I Use &rarr;](/uses.html)

*PS. I plan to add a desktop picture of my complete setup once I find the time!*

