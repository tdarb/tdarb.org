---
id: 459
title: Faking 3D Elements with CSS
date: April 29, 2020

layout: post
categories:
  - Web Design
summary: Tutorial on how to design a pure CSS object that gives the illusion of a 3D element
---

*Although not always practical, creating the illusion* that some of your web elements are &#8220;3D&#8221; can be a fun experiment. I set out to see if I was able to create such an illusion with only 2 HTML elements and as little CSS as possible.

This is what I ended up creating:

<p class="codepen" data-height="612" data-theme-id="light" data-default-tab="result" data-user="bradleytaunt" data-slug-hash="VwvzKyb" style="height: 612px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="CSS Orb">
  <span>See the Pen <a href="https://codepen.io/bradleytaunt/pen/VwvzKyb"> CSS Orb</a> by Bradley Taunt (<a href="https://codepen.io/bradleytaunt">@bradleytaunt</a>) on <a href="https://codepen.io">CodePen</a>.</span>
</p>

## The HTML

Prepare for your mind to be blown:

```html
<div class="main-orb">
    <div class="inner-orb"></div>
</div>
```

That&#8217;s it &#8211; I wasn&#8217;t kidding when I said we would use only 2 HTML elements. The `.main-orb` is the core shape (set to 400&#215;400) and the `.inner-orb` is placed on top of it&#8217;s parent at a slightly smaller size (360&#215;360) &#8211; but more on that below in the CSS portion.

## The CSS

First we give the bigger orb element (`.main-orb`) the default styling needed to represent a 2D circle:

```css
.main-orb {
    background: linear-gradient(#fff 0%, #eee 10%, #2E86FB 50%, #1A237E 100%);
    border-radius: 9999px;
    height: 400px;
    margin: 4rem auto;
    position: relative; /* This is important for the inner orb element later */
    width: 400px;
}
```

Next, we include both `:before` and `:after` pseudo elements for our orb&#8217;s drop shadow. You _could_ do this with a simple `box-shadow` property on the `.main-orb` itself, but I&#8217;ve explained in a previous post why [that&#8217;s not the best approach](/blog/better-box-shadows.html).

```css
/* Shared styling for both pseudo elements - Remember DRY */
.main-orb:before, .main-orb:after {
    border-radius: 200px 200px 9999px 9999px;
    bottom: -10px;
    content:'';
    filter: blur(20px);
    height: 40px;
    position: absolute;
    z-index: -1;
}

/* Bigger, lighter shadow */
.main-orb:before {
    background: rgba(0,0,0,0.4);
    left: 7.5%;
    width: 85%;
}

/* Smaller, darker shadow */
.main-orb:after {
    background: rgba(0,0,0,0.7);
    left: 20%;
    width: 60%;
}
```

With our main orb complete we can move on to the `.inner-orb` element to help bring slightly more depth to our floating ball of CSS:

```css
.inner-orb {
    background: linear-gradient(#fff 0%, #2E86FB 60%, #283593 100%);
    border-radius: 9999px;
    box-shadow: 0 8px 20px rgba(0,0,0,0.5);
    height: 360px;
    filter: blur(18px);
    left: 20px;
    position: absolute;
    top: 15px;
    width: 360px;
}
```

## Poor-man&#8217;s 3D elements

Clearly implementing something like this will never come close to generating true 3D renders on a website, but it is a fun exercise to see how much further we can push simple CSS. Feel free to fork the above CodePen to play around with different colors and shadow placements.

