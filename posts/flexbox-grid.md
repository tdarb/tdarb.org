---
title: Adaptable Flexbox Grid
date: November 22, 2018
layout: post
column: single
description: Learn how to create a very simple grid with flexbox
summary: The days of overly complex grid systems are gone. Learn how to create adaptable
  grid systems with flexbox using barely any CSS.
redirect_from: "/2018/11/22/flexbox-grid/"
---

*You can use `flexbox` for many tricky layout "hacks"* and implementing a grid layout is no different.

Check out the CodePen below to see how you can implement a flexbox grid system that adapts automatically based on how many items you insert per row (maximum 6 children in this example).

<p data-height="426" data-theme-id="0" data-slug-hash="rQdLxv" data-default-tab="result" data-user="bradleytaunt" data-pen-title="Adaptable Flexbox Grid (Pure CSS)" class="codepen">See the Pen <a href="https://codepen.io/bradleytaunt/pen/rQdLxv/">Adaptable Flexbox Grid (Pure CSS)</a> by Bradley Taunt (<a href="https://codepen.io/bradleytaunt">@bradleytaunt</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>


