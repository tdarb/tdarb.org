---
title: "My Changing Opinion on Personal Website Design"
layout: post
date: May 19, 2021
summary: "Let's discuss my love-hate relationship with web fonts."
---

*Hey would you look at that* - my personal blog has been redesigned *again*! Although I am still using good ol' Jekyll for the backend, I have now added a more fleshed-out CSS design which also includes a set of open source custom typefaces.

**Gasp!** "How *could* you?!" I hear you ask. Let me explain.

## Personal sites should feel personal

I can see how this change might seem hypocritical (it took some convincing myself) but I decided to follow in to footsteps of Kev Quirk and [added a little whimsy and character](https://kevq.uk/adding-some-whimsy-character/) to my website. After all, personal websites should *feel personal*. My obsession with barebones HTML &amp; CSS serves its purpose on other public projects, but seems limiting for my own little space on the interwebs.

## Banned from my own club

I had originally converted this blog's design to use *zero* CSS and instead rely solely on default browser styling. The main reasoning for doing so, was to have the ability to include my own personal website in the [XHTML Club](https://xhtml.club) project. (I never said it was a *good* reason)

After giving it some thought, I've decided that this limitation seemed too extreme even for me.

## Moving forward

I know I always say "With this new design, I can finally focus on just writing content!" - but this is a lie. I'll probably be fiddling with my personal website until the day I die. The good news is that I *do* have a few tutorial blog posts lined up to publish soon - so be on the lookout for those!

Thanks for reading through my pointless ramblings about personal websites. It's good to just vent about these things sometimes...

