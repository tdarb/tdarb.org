---
title: Being More Efficient as a Designer and Developer
date: August 28, 2019
layout: post
description: Talking about being more efficient with your time as a designer
summary: Talking about being more efficient with your time as a designer when developing
  new projects or working with clients.
---

*I recently began working on a small side project* (a marketing site / blog for an upcoming UX book I'm writing, but I have nothing to promote yet - sorry) and found myself circling around different static site generators (SSG) in the initial design concepts. The thought of learning an entirely new blogging platform was inspiring and seemed like a good excuse to expand my skillset.

Although I've used 11ty and Hugo in the past for client work, this personal website runs on Jekyll. I'm very familiar with Jekyll and can push out a point-of-concept site in a flash with little-to-no effort. So, why was I looking to jump into a SSG I hadn't used before? 

And that got me thinking... **Why am I moving away from being efficient?**

## Before we begin...

I should preface everything else I'm going to mention in this post with this: *learning new stuff is awesome*. You should expand your knowledge as much as you can, no matter what industry you find yourself in. I've found it to be a great catalyst for boosting my passion in design and development.

Okay, I've made it clear that learning is important to the growth of your career - so please keep that in mind before you read my next statement:

**Just use what you already know.**

By using your current experience (maybe even expertise) with a design system, CSS framework, blogging platform, programming language, etc. you can get something *built*. Not to mention you can get that thing built in a *fraction of the time*. After all, building things is kind of the point of being a designer (or developer), right?

My current side project may be a slight edge case in this regard. Since it's a personal "dev" website, most of the tech stack choices comes down to personal preference - not client requirements. But I believe my point still remains: you shouldn't reach for something new and shiny *just because* it's new and shiny.

## Some vague examples

It might be easier to understand what I mean by using some possible real-world examples:

| Problem | New Way | Efficient Way |
| ---------------------------------------------- | --------------------------------- | ------------------------------------------- |
| A local bakery needs product and e-cart functionality | Learn a new custom ecommerce platform | Use a popular pre-existing library you're familiar with |
| Create an add-on blog for a medical clinic | Try a custom built static site generator and hook in a git-based CMS | Spin up a quick WordPress site and hand-off |
| UI mockups for a workout iOS app | Test out the newest design tool just released | Use your go-to default design tool you (Sketch, Figma, etc)

I know all of this is very much "common sense", but you would be surprised how often we reach out for the latest and greatest tools (we are creative problem-solvers, after-all). If a current project allots you the time to learn a new skillset alongside outputting a quality product - then more power to you. In my experience that's a rare luxury, so my advice is to focus on shipping quality work (whether that's code, design, analytics, content, etc) instead of getting caught up in the "new and shiny".

## But wait, how / when do I learn new things?

It isn't exactly ground breaking to state that you should keep things simple as a developer. There are probably hundreds of posts on the web advocating for the exact same thing - which is good. At the same time, we as designers and developers need to avoid stagnation - something that can happen all too easily.

So how do we learn new things? This is a hard thing to answer. Really, the best response would be: **it depends on the designer / developer**. I know, *what a cop-out*. Unfortunately, it's true. There is no one solution to learning anything new.

The best I can do is offer up some possible options:

<ul>
    <li>Learn outside of work
        <ul>
            <li>Reading / listening to a technical book on your commute or before bed</li>
            <li>Take an online course you can work on after hours</li>
        </ul>
    </li>
</ul>

<ul>
    <li>Contribute to an open source project that you aren't familiar with but are interested in
        <ul>
            <li>Even tiny contributions go a long way, don't doubt yourself so much</li>
        </ul>
    </li>
</ul>

<ul>
    <li>Ask your current company (if not a freelancer that is) to learn on their time
        <ul>
            <li>It's a valid argument that your company should have vested interest in you becoming a better developer / designer</li>
        </ul>
    </li>
</ul>

## Easier said than done

Sometimes, even the suggestions above don't work for certain individuals. Life is hectic and other important things can pop-up taking precedence. Don't let it get you down - there are more important things in life than mastering the newest framework that released 25 minutes ago.

My motto is to keep shipping quality products that you actually give a shit about. Otherwise it doesn't matter how "new" it is.



