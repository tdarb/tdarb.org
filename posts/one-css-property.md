---
title: Billing for One CSS Change
date: November 29, 2019
layout: post
description: Stop selling yourself short as a designer by doing work for free. Every second spent working is billable
summary: Stop selling yourself short as a designer by doing work for free. Every second spent working is billable
---

*Every second you spend working as a designer should be billed* back to the client. A simple button color change? Bill them. Additional links added to an existing menu? Send that invoice over. Some basic typeface changes? Don't do it for free.

You need to be charging for *all* design work, regardless of difficulty or time required.

This concept might seem extremely obvious to more senior level workers but I have seen a good amount of junior devs make the mistake of "working for experience" or better yet "strengthening the client relationship". Early on in my career I was just as guilty of doing this kind of thing. It was and still is a very foolish practice.

## Do you really bill for *one* CSS change?

Absolutely. From the client's perspective it may seem like they are being billed for one CSS change and 30 seconds of a designer's time. In reality, they are paying for the designer's years of experience to be able to solve that problem in *only* 30 seconds.

Would the client be happier with a significantly less qualified designer charging the same amount of money but taking 3 *hours* to complete the task? In the end, what's the difference?

> If it is a simple change that they believe should cost nothing, then why aren't they doing it themselves?

We as developers and designers work in an odd industry. A lot of people (read clients) outside of our bubble tend to think they have a much better understanding of the work we do. Because of this, they tend to preface work requests with phrases like:

- "This should be a simple change"
- "This shouldn't take more than a couple of minutes"
- "This seems like an easy fix"

Most of the time these comments are harmless, but other times they are a subtle way of downplaying your skill and experience required to complete these work items. That skill and experience shouldn't ever come free. It makes you wonder if these same people expect free work from trades-people (electricians, plumbers, etc) when they need what they think is a "simple" fix in their house.

Do you think workers in *most* other industries travel out to someone's home and fix "small" issues for free? Hell no.

So why are developers and designers doing work for free? I truly don't know - but it needs to stop.

## A simple but useful system

You should live by the rule that every *second* you spend working for someone else is billable. I don't mean that you should literally bill per second, but instead round to nearest estimated time slot (whether you are billing hourly, daily, sprint-based or per project). This ensures that your efforts are paid for and that the client will begin to truly value your time. Doing this from the get-go will save you headaches in the future.

I'm sorry if this comes off as preachy, but I'm just trying to inspire designers to value their work more. Don't sell yourself short - your talent is valuable and shouldn't be taken for granted.

