---
title: "89 Blog Posts in a Single HTML File"
layout: post
date: March 22, 2021
summary: "Generating a my personal blog website as a single file via PHPetite"
---

*This is no longer the current setup of my blog*. I have switched back to Jekyll for performance reasons. I'll be leaving this post up as a point of reference though :)

---

This is my personal blog (if that wasn't already obvious). I currently have 89 blog posts living here. But I have done something magical today:

All of these articles are rendered inside a *single HTML file*. 🤯

## What Sorcery is This?!

No magic here - I'm just using my own personal static site generator called [PHPetite](https://phpetite.org). You can find all the code and more information about PHPetite on the [official Github repo page](https://github.com/bradleytaunt/phpetite). Please note that this project is still very much a WIP, so go easy on me when pointing out bugs or issues!

## Pingdom Stats

Before we get into more details on how everything is built, let's take a quick look at some of my website stats (not that pure numbers are 100% accurate of user experience):

- 99 Performance Grade
- 951 ms Load Time
- 231.3 KB Page Size (thank you Netlify compression!)

[See all the Pingdom details for yourself](https://tools.pingdom.com/#5e169d6a82000000)

Now the `Page Size` will certainly change as more blog posts are added over time, but if the total webpage size is roughly 230 KB with 89 blog posts, I figure that gives me the ability to add another 200 or so posts before I cross the [1MB threshold](https://1mb.club)

I'll deal with how to load in the content more efficiently when that times comes...

## What About Images?

Good question. Since a large portion of my articles tend to be focused on design and CSS, visual examples are somewhat important. So how do I avoid loading in all the individual images on every single post into this single HTML "website"?

Easy - I don't load any images at all.

~~I set the default `img` source to point to `about:blank` and rely on the user to click/tap the image to load it in via the `onclick` attribute. For users with JavaScript disabled, I also include a link to the "full size image" in the `figcaption` below the image. No sweat.~~

I now simply set any images that are not included in the current, visible `section` to `display: none`. This avoids breaking things with the RSS feed.

Here is an image example of an old Dribbble shot I created years ago:

<figure>
<div class="img-parent">
<img loading="lazy" src="/placeholder-image.webp" onclick="this.src='/public/images/aqua-ui-css-buttons.webp'" alt="Aqua UI buttons">
</div>
<figcaption><b>Click the placeholder to load in the real image</b><br>Example Dribbble shot for testing. Feel free to click the default image in order to load the correct Dribbble source. <a href="/public/images/aqua-ui-css-buttons.webp">View full size image</a>.</figcaption>
</figure>

I have noticed that Safari / Firefox on iOS renders the imagery as broken image links - I plan to look into this further to avoid that confusion. For now it works well on desktop Chromium and Firefox.

```html
<figure>
    <div class="img-parent">
    <img loading="lazy" src="/placeholder-image.webp" onclick="this.src='https://res.cloudinary.com/bradtaunt/image/fetch/q_auto:low/v1570124593/https://uglyduck.ca/public/images/aqua-ui-css-buttons.webp'" alt="Aqua UI buttons">
    </div>
    <figcaption><b>Click the placeholder to load in the real image</b><br>Example Dribbble shot for testing. Feel free to click the default image in order to load the correct Dribbble source. <a href="https://res.cloudinary.com/bradtaunt/image/fetch/q_auto:low/v1570124593/https://uglyduck.ca/public/images/aqua-ui-css-buttons.webp">View full size image</a>.</figcaption>
</figure>
```

## How's the SEO?

Probably terrible to be honest. One single file for all blog posts is something Google will most likely frown upon. Whatever - it's my personal blog so I don't really give a shit.

## RSS Feed

If you recently added my old RSS feed, I'm sorry, but you'll have to update. The new feed is located at [tdarb.org/feeds/posts.xml](https://tdarb.org/feeds/posts.xml). I made this change to avoid spamming the current feed users.

## Tasty Dog Food

At the end of the day, by moving my personal blog over to PHPetite, it makes me more aware of current bugs and restrictions within the project. I feel like this will help me to improve this static site generator even more over time, instead of letting it die a slow death ignored on Github.

I hope you enjoyed this little breakdown and maybe even want to try out PHPetite for yourself? 😉

