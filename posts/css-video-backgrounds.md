---
title: CSS Video Backgrounds
date: April 16, 2018
layout: post
description: Learn how to add video files as background images in Safari
summary: Safari is actually ahead of the game for once, allowing developers to set
  video files as background property attributes in CSS.
redirect_from: "/2018/04/16/css-video-backgrounds/"
---

With the release of Safari 11.1 on macOS and Safari on iOS 11.3, developers now have the ability to support background videos (mp4 support only - at the time of this article) with pure CSS.

Example:

```css
.video-background {
    background-image: url('path-to-video.mp4);
}
```

## See it in action

Check out the very basic CodePen I created below to see it live (make sure you view it in latest Safari or else you won't see anything :P)

<a href="https://codepen.io/bradleytaunt/pen/JLgrag/">CSS Video Background (Safari Only)</a>

You can read up on all the new features implemented in 11.1 Safari here:

[New WebKit Features in Safari 11.1](https://webkit.org/blog/8216/new-webkit-features-in-safari-11-1/)







