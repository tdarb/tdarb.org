---
title: "A Reality Where CSS and JavaScript Don't Exist"
layout: post
date: November 03, 2021
summary: "As much as I love CSS and JavaScript, it is slowly becoming a tool for evil practices on the web"
imgnum: 0
size: 11.5
---

*This is my personal opinion. Please leave your pitchforks at the door...*

I love CSS. I can spend hours deep diving into a website's CSS system and never find myself getting bored. It's pretty amazing to see the problems other designers are able to solve with just a little bit of custom styling and determination.

I like JavaScript. It serves it's purpose and makes complex functionality on the web easier to wrangle in and understand.

*But I think both should have never come into existence*.

### Heresey!

I know, I know - this website itself uses a little, teeny-tiny amount of CSS. I am indeed a hypocrite - but did I ever claim that I *practice* what I preach? At least this personal website is JavaScript-free... (apart from a handful of CodePen examples embedded in some of the tutorials).

### Moving On...

I'm not a complete idiot. I realize that the web has evolved significantly over the years, and to propose that it should have remained stagnant with it's original concept of "paged documents" is foolish. But that *is* what I'm suggesting - at least, partially.

### Consistent & Boring

Out there in the multiverse is a reality where the web is a complete borefest. Information is the only driving factor to visit a "web page" and PWAs have never come to exist. Custom styling, fancy interactive animations and single-page functionality isn't even something that can be implemented. The web is just a system of HTML/plaintext documents sharing information and data. Users browse the web in quick bursts to satisfy their queries or read something interesting. Then, they return to *real life*.

My goodness what a *beautiful* reality that would be. Consistent, boring and wonderful.

### "Wait - Aren't You a Designer?"

Yes - and again more hypocrisy. My livelihood depends on software requiring custom UIs and properly audited UX flows. By suggesting this change I am throwing myself under the bus and putting myself out of work. All my experience would become worthless and the world of software *design* would cease to exist.

**I would be okay with that**. If it meant the web as a whole was a better place - so be it.

### A Look at the "New World"

Sometimes it is easier to visualize a concept instead of just discussing it. Below you can find an example of a "converted" website[^1] showcasing how sites would look and feel in this design-less reality:

- Wealthsimple: [Current Website](https://www.wealthsimple.com/en-ca/) / [New Version](/etc/html-only/wealthsimple/)

As you can see, all the *fluff* has been removed from the existing design and only the content remains. No scroll-jacking or extra JavaScript libraries are downloaded. Users can easily skim through all the content and screen readers won't struggle through any custom elements. It also loads incredibly fast.

Of course, to our[^2] eyes, this design might look ugly or seem as though the site's CSS failed to load - but in a reality where this is the standard, it is beautifully minimal. Either way, I find that this was at least a fun thought experiment and hopefully leaves you thinking about how the web *could have been* as well.

Now, back to designing UIs for the web...

<hr data-content="footnotes">

[^1]: Selected site based on my own personal preference
[^2]: Referring to current users of the web

