---
title: "Introducing PageRoast"
date: March 11, 2021
layout: post
column: single
summary: "Receive a detailed report analyzing your landing page with actionable items to improve your conversion rate."
---

*Following up with my concept of releasing small side projects weekly*, I have officially launched [PageRoast](https://pageroast.com). What is PageRoast I hear you ask?

> Receive a detailed report analyzing your landing page with actionable items to improve your conversion rate.

In simple terms that just means I will **roast your landing page**. Included in a page roast report is:

- Fully detailed ABCD Framework report
- Proposed content & design changes
- Page performance audit

And you get all of this for just **$100** (currently 50% until April 1st with coupon code PHROAST). Alright, enough with the "sales pitch" - why did I make this side project?

From the main [launch blog post on PageRoast itself](https://pageroast.com/#2021-03-10-pageroast-has-launched) I wrote:

> I've always been obsessed with developing landing pages that produce high conversion rates. It's what I've done for a large portion of my web development career.<br><br>So I thought to myself, "Why not launch a small-scale audit service to help indie devs and startups?"

That's really all there is to it. If you would like to learn more, check out the following links:

- [PageRoast main website](https://pageroast.com)
- [PageRoast official launch blog post](https://pageroast.com/#2021-03-10-pageroast-has-launched)

