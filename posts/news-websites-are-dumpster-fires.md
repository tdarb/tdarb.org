---
title: News Websites Are Dumpster Fires
date: May 29, 2019
layout: post
description: News websites are absolute garbage thanks to spamming advertisements
summary: News websites are absolute garbage thanks in part to vendors spamming advertisements
  and blackhat, clickbait tactics.
redirect_from: "/2019/05/29/news-websites-are-dumpster-fires/"
featured: true
---

*Online news outlets are a dying breed and many users* have decided to consume information elsewhere. Why? Because the news industry has become a cesspool of anti-consumer and blackhat practices that has eroded trust for the sake of money.

## What news sites get wrong

I could write up an entire essay about all the shady practices that *most* news sites are guilty of, but here are just a few top level issues:

- Clickbait headings with misleading information
- Disabling the user from reading if ad-block is present
- Tracking the user with 3rd party scripts
- Taking massive performance hits (specifically on mobile due to huge JavaScript blocks)
- Pop-up ads
- Fixed headers or footers which leads to harder readability / accidental element interactions

## But they need ad revenue!

If your business is solely dependent on tracking scripts, tricking users with clickbait titles and using archaic ads - then you're destined to fail regardless. These practices create an unsafe and unhealthy web for everyday users - not to mention most browsers have announced that future updates [will be blocking ads by default](https://support.mozilla.org/en-US/kb/content-blocking). *News outlets need to adapt or die*.

## What's the solution?

I don't have a *fix all* band-aid to replace current revenue streams for news websites. I'm sure someone much smarter than I can come up with better ideas, but just off the top of my head:

- Switch over to a monthly subscription plan (if no one pays for it maybe you weren't as useful of a source as you thought)
- Partner with brands to create sponsored articles (without ruining the user experience of course)
- Place a larger emphasis on user donations or promotions

## The News Shouldn't be Spam

Most traffic flowing into news websites are there for one thing: *the content*. News outlets should not be spamming their main revenue supply (**the users**) or misleading people with false information.

If you're a regular consumer of news and you happen to run across a platform that is guilty of any of these practices, shoot them an email explaining why you won't be returning to their website (unless they change their ways). These anti-consumer practices will only stop when these organizations start losing money.

