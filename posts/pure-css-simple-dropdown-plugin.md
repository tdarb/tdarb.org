---
title: Pure CSS Simple Dropdown Plugin
date: September 20, 2018
layout: post
column: single
description: Learn how to implement a custom select dropdown with CSS
summary: A simple, JavaScript-free way to implement a custom styled, plug-and-play
  select dropdown with pure CSS.
redirect_from: "/2018/09/20/pure-css-simple-dropdown-plugin/"
---

I find myself blowing away default browser `select` styling and implementing my own custom dropdowns far more often than I'd like. So, I recently created a very simple and clean component using just pure CSS.

Check out the CodePen below and feel free to morph it as you see fit for your own projects!

<p data-height="265" data-theme-id="0" data-slug-hash="rZPzWy" data-default-tab="result" data-user="bradleytaunt" data-pen-title="Plug & Play Dropdown (Pure CSS)" class="codepen">See the Pen <a href="https://codepen.io/bradleytaunt/pen/rZPzWy/">Plug & Play Dropdown (Pure CSS)</a> by Bradley Taunt (<a href="https://codepen.io/bradleytaunt">@bradleytaunt</a>) on <a href="https://codepen.io">CodePen</a>.</p>

<script async src="https://static.codepen.io/assets/embed/ei.js"></script>


