---
title: Web Development on a $200 Chromebook
date: January 07, 2020
layout: post
description: Detailing my switch over to Chrome OS from the Apple ecosystem
summary: Detailing my switch over to Chrome OS from the Apple ecosystem
---

*This blog post was written, edited and tested locally* on a cheap $200 Chromebook. The article draft was composed in Sublime Text. Jekyll (the SSG this website uses) was generated via the Linux Beta Terminal running alongside Chrome OS. It was then pushed to the Github repo from the same Terminal and published automatically to Netlify. But more on that later.

First, we need to ask an important question: why use a Chromebook for web development? Maybe a better question might be: why not?

## Mild interest turned into an experiment

Looking from the outside, I always liked the concept of the Chrome OS ecosystem. It had a solid list of features that appealed to me, specifically:

- Blazingly fast boot times
- Long lasting battery life (roughly 8 hours with consistent usage)
- Extremely cheap entry point for "low-end" devices
- And, more recently, support for Android &amp; Linux apps

The obvious downside to using a Chromebook is the lack of a "real" operating system. A Windows, Mac or Linux machine will always have more flexibility and depth than a simple Chromebook. But I've found that simple is all I need - or want.

**Side note:** *I wrote an article not to long ago about [blocking Google from my website](https://uglyduck.ca/stop-crawling-google/) and I can see how this current post may come across as hypocritical. Since the search/AMP teams are not that same as the Chrome OS / Chromebook development team, I can view each of those projects based on their own merits. If you still view me as a hypocrite, that's fine* :)

## My use case (YMMV)

I ended up grabbing the [Acer 11 N7 Chromebook](https://amzn.to/2Xvx20q) off of Amazon for $200 (on sale and in Canuck bucks). It comes packed with 4GB of RAM with an Intel Celeron (1.6GHz) and 32GB of storage. Fairly barebones stuff.

My day-to-day use cases are fairly straightforward. I work with git, static site generators, WordPress locally and on staging servers, edit documents, send emails and watch stupid YouTube videos. You know, the basics. My dirt-cheap Chromebook handles all of these things with ease.

My Chromebook setup is as follows:

- Linux Beta (ships with `git`, etc)
- Sublime Text (you can always use VSCode if you prefer)
- Terminal
- Local by Flywheel (local WordPress development)
- Figma in Chrome (UI design / mockups)

That's it. I understand other developers may require a lot more than this, but with Linux running in a container alongside Chrome OS you can pretty much run anything you could possibly need (excluding exclusive Mac &amp; Windows apps, of course).

## Can a $200 Chromebook be your next web dev machine?

For me? Yes. For you? Maybe.

I mean, it really just depends on what you need your machine to accomplish. If you can be productive and the apps you use are just as fast and responsive as they would be on a more robust OS - then I say go for it. You don't even need to be a cheapo like me, instead you can snag a fancier Chromebook or Google's own Pixelbook. There are a ton of options when it comes to Chrome OS based devices.

If you wanted to experiment to see if the Chrome OS ecosystem could work with your development flow, you can always check out something like [CloudReady](https://www.neverware.com/freedownload) to test on your current machine before pulling the trigger.

In the end, it doesn't matter what operating system or apps you use to get your development work done - so long as it is productive and easy-to-use.

For my current situation, all I need is my cheap little Chromebook.

