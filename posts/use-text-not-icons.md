---
title: "Icons Should be Complementary - Text is Always Better"
date: December 17, 2021
---

Designing[^1] software is a complex thing. A great deal of real-world testing and user feedback is needed to create the best solution to the problem you are trying to fix. Obvious requirements are to keep things simple, make it easy to understand by *looking* at it, and build it to be headache-resistant for future updates. All these things are easier said than done. This is the challenge of a designer's dat-to-day.

But with this term of "simplicity" modern designers tend to take this approach too much to heart. In my 12+ years involved in UI/UX software design, I have lost count how many initial iterations of interfaces suffer from the same "dumbing down" decision making:

**Using icons to represent an action or function without textual information**.

If you decide to stop reading the rest of this article, at least take away this one important thing:

> *Always try to use text to convey your designs*

After achieving this, you should start reiterating those designs to include iconography. Even then, not all UI instances will require you to do that. Designers will find this process difficult, which is why it is important to get *right*.

## Icons make an *ass* out of *u* and *me*

Icons make general assumptions about what the user may or may not understand. Leading with this in your designs will end *poorly for you*. Trust me - I've learned this through failed designs many times over. A certain visualization might be common knowledge to you, while differing greatly to someone else with a different set of experiences.

I've found the only thing you should ever *assume* is that the user knows nothing. Please note - I'm not referring to their intelligence but instead their software literacy.

Take a look at our now "famous" save icon used in almost every piece of software; the floppy disk. Do any software users below the legal drinking age even understand the initial reasoning for using this icon? In all honesty, it was a terrible icon decision even when first introduced. No "hard copy" of the save action is taking place, software creates this save in a digital space[^2]. Yet, it was adopted and people (ie. designers) went along with it.

**Quality is not measured by mass appeal.**

The argument could be made "People learned to associate "Save" with a Floppy Disk icon..." and my response would be "But what alternatives were they given?"

Original software designers (and developers) held all the power in early UI decision making. General users didn't *know* any better. Things were new and fresh. Now our response is to shrug our collective shoulders and say, "That's how the save icon has to be now!"

Hogwash. Make it a button that says, `Save File`. I'm not kidding. Oh, it doesn't work with your current design? Then your initial design wasn't future-proof then, was it? I sound snarky here but many designers put up imaginary walls around their design systems, making them incredibly rigid and difficult to adapt.

Take the time to do even a small thought / wireframe experiment: redo the layout and flow of your application without using a single piece of iconography. If you can't achieve this with even limited success, something is wrong with the design.

## The hamburger menu is the 7th circle of Hell

Normally, the inclusion of a hamburger menu is indicative of an overly complex application. Too many cooks and all that jazz. Enterprise applications don't get a pass here either, as they tend to be the worst culprits of pouring out everything on to the user as software vomit. Sweeping all this interaction under the hamburger "rug" does not make for a cleaner design.

New features are great, but stop dumping so much of it behind hidden, unintuitive sub-navigation. This design is such a "quick fix" and plagues far too many software apps[^3]. Both desktop computers and mobile devices allow users to *scroll*, let them.

I've discussed this in further detail here: [Using Hamburger Menus? Try Sausage Links](https://tdarb.org/hamburger-menu-alternative/)

## But what of the "advanced" users?

I understand applications will have advanced or "pro" users that have full knowledge of the product and wouldn't need things *spoon fed* to them. This is a more difficult problem that I myself haven't been able to solve without approaching each one on a case-by-case basis. Unfortunately, there is no "one size fits all" method to this. But, although solving for advanced users proves difficult doesn't mean we should dismiss the merits of avoiding icons as a crutch.

## Try for yourself

As I stated above, try doing a quick design experiment by replacing all your existing iconography in your application with simple text. I assure you that at least you'll discover interesting design flaws in your system.

[^1]: By "design" I'm referring to visuals not programming or system engineering
[^2]: Early software programs did save to an external floppy disk. My point stands that many digital file storage applications copied this iconography blindly.
[^3]: Not to mention how rampant it is on plain ol' regular websites. If you're hiding five menu items behind a hamburger menu for "mobile users", you're doing it wrong.
