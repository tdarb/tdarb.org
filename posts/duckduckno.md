---
title: Two Weeks with the DuckDuckGo Browser
date: May 17, 2022
---

After using the beta apps for the new DuckDuckGo browser for both macOS and iOS, I have returned to Safari.

This switch back doesn't mean that these browsers are _bad_ by any means. Both browsers are decently fine for casual users. Unfortunately, they aren't ready for prime time "power" users. I'm happy to see another company jump into the browser market (and one that is using WebKit instead of another Chromium clone) but for my day-to-day needs it doesn't cut it.

Let's break things down:


### The Good

- Clean UI with a mix between Chrome and Safari
- The "Leave No Trace" fire button to purge data, cache and cookies
- Feels snappy and responsive
- Based on WebKit NOT Chromium


### The Not So Good

- No sync options between systems (that I could find)
- No extension support

Now you might look at this list and notice there are more listed points set under the `Good` category. Keep in mind that quantity does not always equal quality. The main negative of not allowing user extensions cannot be overstated. I think that when building a browser in this $CURRENT_YEAR it's imperative to allow for total user control. Don't wall things off. Don't _assume_ you know best for your users.

This is a huge issue since a good majority of friends, family and coworkers I talk with use more elaborate ad-blocking. One of the first things I tested with the DuckDuckGo browser on desktop was to watch a YouTube video. I was immediately slammed with multiple ads - both in video form and as banner pop-ups.

Overlooks like this shouldn't happen. This is UX 101.

Another obvious overlook is history and account syncing across devices. Unless I missed something obvious, I could not figure out how to seamlessly sync content from my macOS browser to my iOS one. Again, this function greatly improves the experience of jumping between desktop and mobile clients. Don't make me think![^0]


### Closing Thoughts

I appreciate the effort from the DuckDuckGo team. I'm certain this project will get better over time, as they are listening closely to user feedback during the beta. With future improvements I could see myself giving things a second chance.

As it stands now, this feels like something that should be an extension[^1], not a standalone browser.


[^0]: [https://sensible.com/dont-make-me-think/](https://sensible.com/dont-make-me-think/)
[^1]: Technically this already exists as a Safari extension [here](https://apps.apple.com/us/app/duckduckgo-privacy-essentials/id1482920575?mt=12)

