---
title: "The Linux Desktop is Hard to Love"
date: July 14, 2022
---

I want to love the "Linux Desktop". I really do. But I've come to the realization that what I love is the *idea* of the Linux Desktop. The community. The security and core focus on open source. The customizable environments. Tweaking as much or as little of the operating system as I please!

I just can't *stick with it*. I always end up back on macOS. And I'm starting to understand why.

### What the Linux Desktop Gets Right

To be fair, there is an incredible amount of things that the Linux desktop does really well:

- Complete user control
- Ability to drastically change the desktop UI
    - Gnome, KDE, XFCE, etc.
- Overall good and welcoming communities
- Extensive documentation for almost everything

These things make Linux a solid experience overall - but not a *great* one...

### What the Linux Desktop Gets Wrong

If I had to summarize in a word what Linux lacks compared to macOS it would be: *cohesion*.

Apple's macOS keeps a solid consistency throughout its entire design. Everything looks and feels like it is part of the same system. Which is what a fully-fledged OS *should* feel like. The argument can be made that macOS suffers some fragmentation with things like `homebrew`, applications directly from developers vs. applications via the Mac App Store.

While this is true, I believe Linux desktops suffer far worse in terms of fragmented systems. Users building applications from source, `snap` packages, `flathub` packages, custom package managers shipped with separate distros, etc. And with this fragmentation comes the constant debates and discussions around which to use and which to avoid.

This can become overwhelming for average computer users. This is something we tend to forget in our "tech hubs". Most users want to boot up their machine and get to work. Linux can absolutely do this, but if a user hits a minor snag, then I guarantee they will have more difficulty fixing it compared to an issue found in macOS.

### User Experience

Design is important. The user experience will make or break an operating system. This is another issue I've found with many Linux desktops.

Let's take Bluetooth for example. It works flawlessly in macOS. I have never had a single device bug-out or refuse to connect. Devices connect almost immediately when pairing. The UI is intuitive and gives the user clear feedback to what the system is doing while pairing, disconnecting, and so on.

Now, compare this to an average Linux DE experience - not so seamless. The fact that some distros require you to hop into a terminal in order to properly configure Bluetooth is pretty terrible. Sure, most have GUIs setup similar to that of macOS, but I find myself time and time again needing to pop open that trusty ol' Terminal. This is fine for someone like myself, but for the average computer user? No way.

Looking for another example? Printers. Yes, printers are terrible machines created in the depths of Hell itself, but they are a necessary evil. And again, macOS handles "plug-and-play" printer functionality like a champ. Linux on the other hand is a mixed bag. I've had some luck with specific Linux distros working with printers in this "plug-and-play" fashion, while others become a battle of attrition[^1]. Let's not even begin to talk about wireless *only* printers and setting up their proper drivers on Linux.

### Quality Hardware

Another advantage macOS has over most other Linux desktops is tailored hardware. Apple produces the hardware created to run their own operating system, meaning it was specifically built for that task. Linux desktops are designed to run on almost any[^2] piece of hardware. Though this is fantastic in terms of technological sustainability (avoids dumping old devices when they lose "support") it ends up causing more support issues. Needing to support such a wide range of chip sets and drivers spreads the focus on a streamlined UX a little more thin. It becomes difficult to perfect a cohesive experience user-to-user when some many variables can be different. I should note that some distros[^3] are making fantastic strides in this area but are still far from ideal.

### I Still Use Linux

I might have attacked the overall Linux desktop experience in favor of macOS a little harshly in this post, but it's a simple reflection of a individual who has used both extensively. I still work with multiple Linux machines daily. I still *like* using Linux.

I just don't *love* it.

[^1]: People swear by CUPS working well on Linux, but this has caused issues for me in the past as well. Unsure why macOS handles it fine...
[^2]: Depending on the desired distro, resources required, etc.
[^3]: A couple that come to mind are Zorin OS and elementary OS