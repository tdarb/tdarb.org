---
title: "ThriftyName: $5 Brand Names"
layout: post
date: February 25, 2021
column: single
summary: "I have officially launched an amusing side project that creates brand names for $5 a pop"
---

*It's been a while since I've written anything on this blog*, but for good reason - **I've been working on a handful of side projects**. I plan to drip-feed release these projects over time, but for today I'm announcing [ThriftyName](https://thrifty.name).

## What is ThriftyName?

In case this post title wasn't clear enough (or you avoided going to the product site itself) ThriftyName is a place where indie devs, start-ups or entrepreneurs can go to get a custom brand name for **just $10**.

This "service" started out as an almost gimmicky joke in my head, but once I began building out the main website I realized that this could be quite useful for cash-strapped indies. After all, not all developers *love* to sit around wasting precious time thinking about product names, when they could use that time to *build their product*.

## Learn More

If you're really interested in the reasoning behind making this project, check out the [official about page on the site itself](https://thrifty.name/#about). I go into a little more depth about my thought process.

## More Side Projects Incoming...

Like I said at the beginning of the post, keep a look out for my other side projects that I'll be rolling out slowing. I'm still not sure of the best "method" to do this (release one every week? every month?) - but I'll figure it out as I go along.

Thanks for reading!

