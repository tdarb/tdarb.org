---
title: Open Source Typeface Pairings
date: January 25, 2018
layout: post
description: A few examples of excellent open source typeface pairings
summary: Using premium fonts in your projects is wonderful, but can sometimes fall
  outside your budget. Here are a few of my personal favorite open source typefaces.
redirect_from: "/2018/01/25/open-source-typeface-pairings/"
---

I always love finding new typeface pairings to use across my personal and client projects, but I find many suggested pairings come with a hefty price tag (rightly so - premium typefaces are normally always worth their cost).

So, I've curated this personal list of 5 exceptionally beautiful typeface pairings that will cost you absolutely <i>nothing</i>. Open source FTW.

## ET-Book &amp; Gill Sans

Download: <a href="https://github.com/edwardtufte/et-book">ET Book</a>, <a href="https://www.wfonts.com/font/gill-sans-std">Gill Sans</a>

<figure>
    <img src="/public/images/et-book-gill-sans.webp" alt="ET Book Gill Sans">
    <figcaption>ET-Book &amp; Gill Sans are based off the font pairings of my personal Jekyll theme: <a href="https://bradleytaunt.com/et-jekyll-theme/">ET-Jekyll Theme</a>.</figcaption>
</figure>

## Playfair Display &amp; Roboto

Download: <a href="https://fonts.google.com/specimen/Playfair+Display">Playfair Display</a>, <a href="https://fonts.google.com/specimen/Roboto">Roboto</a>

<figure>
    <img src="/public/images/playfair-roboto.webp" alt="Playfair Display Roboto">
    <figcaption>Playfair Display &amp; Roboto I find work really well for microblogs or short essay format posts.</figcaption>
</figure>

## Karma &amp; Open Sans

Download: <a href="https://fonts.google.com/specimen/Karma">Karma</a>, <a href="https://fonts.google.com/specimen/Open+Sans">Open Sans</a>

<figure>
    <img src="/public/images/karma-open-sans.webp" alt="Karma Open Sans">
    <figcaption>Karma &amp; Open Sans give readers a little more breathing room between characters. Good choice if trying to keep accessibility in mind.</figcaption>
</figure>

## Libre Baskerville &amp; Oswald

Download: <a href="https://fonts.google.com/specimen/Libre+Baskerville">Libre Baskerville</a>, <a href="https://fonts.google.com/specimen/Oswald">Oswald</a>

<figure>
    <img src="/public/images/libre-oswald.webp" alt="Libre Baskerville Oswald">
    <figcaption>Libre Baskerville &amp; Oswald oozes character and takes inspiration from a more print-based medium.</figcaption>
</figure>

## Fanwood &amp; League Spartan

Download: <a href="https://www.theleagueofmoveabletype.com/fanwood">Fanwood</a>, <a href="https://www.theleagueofmoveabletype.com/league-spartan">League Spartan</a>

<figure>
    <img src="/public/images/fanwood-league.webp" alt="Fanwood League Spartan">
    <figcaption>Fanwood &amp; League Spartan paired together allow the main content to be easily readable, while the headers instantly grab the user's attention.</figcaption>
</figure>


