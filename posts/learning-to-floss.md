---
title: How to "FLOSS" as a Web Designer
date: February 07, 2020
layout: post
description: A detailed breakdown of how to only use free-and-open source software as a web designer
summary: A detailed breakdown of how to only use free-and-open source software as a web designer
---

*I have a profound respect for the open source community*. I most likely wouldn't have the skills or knowledge I do today with it. Unfortunately, when I was just starting out in "web dev" some 10 years ago, proprietary software was the main go-to for a newbie designer. 

Dreamweaver. Fireworks (those were the days). Photoshop. Illustrator. Adobe products basically had a stranglehold on the web design community. Any company you joined at the time more than likely required you to know and use these tools, making it hard for designers to try out new software for their day-to-day needs. Not to mention the *cost* associated with these products.

Fast forward a handful of years and we have a lot more options in terms of design / development software (whether Windows, Mac or Linux). UI design tools like Figma, Sketch and Adobe XD have since popped up and become the most popular among designers. New text editors and terminals hit the scene, fighting for developer attention (Atom, Sublime Text, VSCode, LightTable, iTerm2, Hyper, etc.).

But what if a designer wanted to go completely <span class="help-text" title="Free/Libre and Open Source Software">FLOSS</span>?

Below you will find my own personal list of FLOSS applications I use across design, development and simple document management:

<h2 id="floss-apps">FLOSS Application Breakdown</h2>

### Web Browsers
- Firefox [https://www.mozilla.org/en-CA/firefox/](https://www.mozilla.org/en-CA/firefox/)
- Brave [https://brave.com/](https://brave.com/)

### Visual Design
- GIMP [https://www.gimp.org/](https://www.gimp.org/)
- Inkscape [https://inkscape.org/](https://inkscape.org/)
- Pencil [https://pencil.evolus.vn/](https://pencil.evolus.vn/)

### Code / Text Editors
- Brackets [http://brackets.io/](http://brackets.io/)
- Visual Studio Code [https://github.com/Microsoft/vscode](https://github.com/Microsoft/vscode)
- Atom [https://atom.io/](https://atom.io/)

### Terminal Shells
- Terminus [https://eugeny.github.io/terminus/](https://eugeny.github.io/terminus/)
- Hyper [https://hyper.is/](https://hyper.is/)

### Documents
- LibreOffice [https://www.libreoffice.org/](https://www.libreoffice.org/)

## Nothing special

This small list I've compiled isn't earth-shattering by any means, but I wanted to put this out into the world for any newbie designer that might be starting out. You don't need to spend a ridiculous amount of money just to get your feet wet in the design industry. By using the tools listed above, beginners can get a solid head-start on creating for the web without burning a hole in their pocket. 

