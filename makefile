.DEFAULT: build

.PHONY: build
build:
	bash pblog.sh > _output/pages/feeds/posts.xml
	xsltproc _output/pages/feeds/posts.xml > _output/pages/blog/index.html

serve: build
	python3 -m http.server --directory _output/pages/
