#!/bin/sh

# Site specific settings
###################################################################################
DOMAIN="https://tdarb.org"
TITLE="tdarb.org"
DESCRIPTION="The personal blog of designer Bradley Taunt"
COPYRIGHT="Copyright 2022, Bradley Taunt"
AUTHOR="hello@tdarb.org (Bradley Taunt)"
OS="BSD" # "Linux" for Linux, "BSD" for BSD Systems (including MacOS)
INC_HEAD_FOOT=true

# Blog structure settings (most users should use these defaults)
###################################################################################
TOC=true
SYNTAX=true
PAGES_DIR="pages/"
POSTS_DIR="posts/"
PAGES=$(find $PAGES_DIR -type f)
POSTS=$(find $POSTS_DIR -type f)
WEB_HTML="blog/"
OUTPUT="_output/pages/"
TIME=$(date +"%T %Z")
TTL="60"
