---
title: Things I Use
subtitle: Collection of hardware and software I use daily
toc: false
---

This page contains a somewhat up-to-date listing of all hardware and software that I currently use. Seeing similar pages on other people's personal websites have proved quite valuable to me, so I figured I would do the same!

### Hardware

- Mac Mini (2012)
    - RAM: 12GB
    - Storage: 256GB SSD
    - OS: MacOS Monterey (OpenCore)

- Kensington Ergonomic Mouse
- Magegee Mechanical Keyboard (blue switches)
- Samsung 27" 4K Monitor

- iPhone SE (2020)
    - Storage: 64GB
    - OS: iOS 15

- Multiple Raspberry Pi devices
    - RPi Zero WH x 2
    - RPi Zero
    - RPi 400

### Software

- Sublime Text
- Safari
- Firefox (for testing purposes)
- Chrome (for testing purposes)
- Apple Mail
- NetNewsWire
- Slack (sadly)
- Figma
- Sketch
- Transmit
- Rectangle
- Secrets
- iTerm
