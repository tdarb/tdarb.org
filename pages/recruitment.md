---
title: "Recruitment Policy"
subtitle: "Please read before sending unsolicited emails"
toc: false
---

The goal of this policy is to save time for both recruiters and myself. By getting all the important details out of the way first, we can save a significant amount of time wasted in introductory 1-on-1 meetings or back-and-forth email chains.

If you wish to "cold email" me, then please be prepared to share all details about the position or opportunity in question. You need to be upfront about the salary (or hourly rate) you are willing to provide for my services.

### Availability & Requirements

I currently have a full-time, salaried position. New opportunity inquiries are okay, but it should be stated that I am quite happy with my current job. However, I am open to work on a side-contract, per-hour basis. My standard rate is listed below:

- $150/hour

**This rate is non-negotiable**.

Thank you for taking the time to read through this policy.