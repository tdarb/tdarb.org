---
title: "Donations"
subtitle: "Help support this humble blog"
toc: false
---

If you feel the burning desire to help support this small blog of mine, please take a look at the options below:

### Standard Donations

- [Directly through Stripe](https://buy.stripe.com/eVa14A885elO9JS9AE)

### Crypto Donations

- Monero

```
4ArUFuSxHZmLZg3iTPJuf74AbxpGHc5m9VxAQxVNVWhT13cZQSfLFyyabNpeAdfX68VgXtkNCDJ3XXTbwG5y7btR4sjhMYL
```

<img src="/public/images/monero-qr.jpeg" alt="Monero QR Code">

