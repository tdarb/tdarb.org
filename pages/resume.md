---
title: "Bradley Taunt"
toc: false
---

*Senior Product Designer*

---

## Education

**Durham College** / Multimedia Design, Associates Degree / 2006-2009

## Experience

**Donorbox / 2021-Present**

Senior Product Designer for internal, administrative, and customer facing user interfaces.

- Conduct user and customer interviews
- Collect and analyze user interactive datasets
- Implement new designs in Figma
- Create interactive prototypes for internal testing
- Sync and test development work with front-end team

---

**Purism / 2019-2021**

Lead designer and marketer for all main Purism products. Also handled all social media.

- Product pages
- WordPress / WooCommerce management
- Mini-sites
- One-off promo pages
- SEO-targeted landing pages
- Social media management

---

**Benbria / 2012-2019**

Lead product designer for all Benbria's properties; Loop, Pulse, Web Feedback

- Redesigned entire customer inbox (web app) journey
- Built the customer-facing UI for clients such as A&W, Royal Caribbean International, Sandals, Delta, Farm Boy, and more
- Worked with iOS developers to port existing web app to native for iPads/iPhones

---

**Netvatise / 2009-2012**

Senior designer and SEO technician for a wide range of clients.

- Static website designs
- WordPress / WooCommerce management • SEO content updates / performance boosts
- Web analytics reviews & audits

## Skills

A brief list of my skill set:

- HTML / CSS / JS
- WordPress / PHP
- Git / CLI
- Figma
- Sketch
- Photoshop
- UI / UX Design
- Performance optimization
- Excellent written communication

## Extra

This resume is intentionally bare-bones. If you have real interest in exploring my experience, expertise and past design work - then let's [talk](mailto:hello@tdarb.org).
