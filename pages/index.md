---
title: "Better User Experience"
subtitle: "I can help improve your product's user experience with a focus on performance, accessibility, and minimal design."
toc: false
---

My name is Bradley Taunt, and tdarb.org is my personal blog. I'm a user interface and experience designer. In my spare time I also dabble in a few open source projects. You can learn [even more about me](/about.html) if you're interested.

This small piece of the internet is where I write about macOS, design, CSS, web development, digital privacy, Linux and whatever else tickles my fancy. Articles and opinions posted here are my own.

### My Work

**I don't keep an up-to-date portfolio**. Most contract gigs and previous full-time positions are heavily protected by NDAs. If you're interested in my detailed experience and UX expertise, I suggest you take a look through my [blog posts](/blog).

You can also open a conversation by emailing me using the <span class="bb">big blue button</span> below. If you are a recruiter looking to "cold email" me, I suggest you take a look at my official [recruitment policy](/recruitment.html) first.

<a class="button" href="mailto:hello@tdarb.org">Say Hello</a>

### Side Projects

A select few of my more "popular" side projects:

- [pblog](https://pblog.xyz)
- [1MB Club](https://1mb.club)
- [XHTML Club](https://xhtml.club)
- [Shinobi](https://shinobi.website)
- [PHPetite](https://phpetite.org)
- [Vanilla CSS](https://vanillacss.com)

### Across the Internet

- [sourcehut](https://git.sr.ht/~tdarb/)
- [Github](https://github.com/bradleytaunt)
- [Fosstodon](https://fosstodon.org/@tdarb)
- [HackerNews](https://news.ycombinator.com/user?id=bradley_taunt)
