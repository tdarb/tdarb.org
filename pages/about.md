---
title: "About: Director's Cut"
subtitle: "The man behind the website"
toc: false
---

If you're reading this page, that means you wish to know more than what I mention on the homepage.

I'm a user experience designer with over 12 years of professional experience across large enterprise application software, all the way down to mom-and-pop shops. I have a strong emphasis on creating intuitive interfaces with a focus on accessibility, performance and minimalism.

When I'm not pushing pixels or hacking up code, you can find me experimenting with random open source side projects, splitting batches of firewood, cheering on the Ottawa Senators, or hanging out with my wife and 3 children.

Articles written here are more or less for my own reference in the future, don't take them to heart.

> "Simplicity is a great virtue but it requires hard work to achieve it and education to appreciate it. And to make matters worse: complexity sells better."
>
> *Edsger Wybe Dijkstra*

I also have a page dedicated to [showcasing the hardware and software I use daily](/uses.html).

### Why "tdarb"?

`tdarb` is my first name and last initial spelt backwards:

> Brad T.<br>
> tdarb<br>
> pronounced tee-darb

Pretty boring.
