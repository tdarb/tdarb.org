<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="../favicon.png">
    <title>Better Search Results | Bradley Taunt</title>
    <link rel="stylesheet" href="https://tdarb.org/etc/better-search-results/style.css">
</head>
<body id="main-body">
    <header>
        <div class="container">
            <h1>Better Search Results<span class="blinking-cursor">|</span></h1>
            <p class="center">A UX experiment and case study re-thinking the design of search result layouts <br>Published by <a href="https://tdarb.org">Brad Taunt</a></p>
        </div>
    </header>
    <main>
        <h2>Searching the web</h2>
        <p class="multi">"Searching" is the most common action performed on the internet everyday. Google alone receives <a href="https://ardorseo.com/blog/how-many-google-searches-per-day-2019/#targetText=This%20translates%20to%20over%2040%2C000,listed%20by%20Internet%20Live%20Stats.">40,000 search queries every second (3.5 billion searches per day on average)</a>. That is a <i>lot</i> of queries. That also means there is a great deal of user interaction taking place between what is essentially a single text input and a list of outputted data. So, why has barely any UI/UX improvements been introduced over the years? I find it difficult to assume that the current "search engine design" has achieved <i>perfection</i> since there are so many obvious flaws.</p>
        <p>Let's take a stab at enhancing this extremely important user interface and see if we can evolve the search engine experience.</p>
        <h2>Default search engine results (DuckDuckGo)</h2>
        <p>For our baseline example we will use the <a href="https://duckduckgo.com/">DuckDuckGo</a> search engine. If you aren't currently using DDG as your default search engine, I recommend that you make the switch. Google has continually proven that they shouldn't be trusted with user data, nor do they have their customers' well being and privacy at the forefront of their priorities. I've <a href="https://bradleytaunt.com/stop-crawling-google/">talked about ditching Google in the past</a> if you're interested. &lt;/End rant&gt;</p>
        <p>This is what a default search result for the term "canada" returns:</p>
        <div class="img">
            <img src="https://tdarb.org/etc/better-search-results/default-search-results.png" alt="Default Search Results">
        </div>
        <p>Overall the design is clean and focuses on exactly what the user cares about: the content. A quick, detailed pane is presented to the right of the top list results, giving the user an easy overview of the searched query. I have no real complaints about the visuals. The overall UX is a different story.</p>

        <h2>Respecting user limitations</h2>
        <p class="multi">Not everyone has unlimited data usage or fast internet. In fact, reliable internet is more of a first-world luxury that is often taken for granted. Many people (specifically those on restricted mobile plans or low-end, under-powered devices) can't afford to eat through their data caps when performing something as simple as an internet search. For some, these under-powered devices may be their only way to connect to the web. By forcing items like tracking scripts or injected advertisement media onto the users' devices, you're stealing away what little available data they have. You're also doing this while providing zero positive gain for the users themselves.</p>

        <h2>Introducing page size results</h2>
        <p class="multi">Why not give the user the ability to instantly know how much data each listing will consume <i>before</i> they commit to actually visiting that website? By introducing a simple toggle filter at the top of the search results (I would advocate to have this <i>active</i> by default) you give the user a quick-and-easy way to see each listing's total page size. Take a look at the concept of this idea below:</p>
        <div class="img">
            <img src="https://tdarb.org/etc/better-search-results/page-size-results.png" alt="Page Size Search Results">
        </div>
        <p>In the top right section of each listing the user can now see how "heavy" each webpage item is. By displaying the page weight, users have a better understanding of which sites will be faster and easier on their data caps - based on a very simple addition.</p>
        <p>An indirect consequence (but overall a great bonus) of this page weight element being standard, is companies would be forced to review their current websites/apps, as to avoid being flagged.</p>
        <div class="img">
            <img src="https://tdarb.org/etc/better-search-results/search-item-details.png" alt="Search Item Details">
        </div>
        <h3 style="color:#002CFC;">&bull; Page Size Indicator</h3>
        <p>This element would show the final size of the entire webpage the listing is linked to. Default styling would display this as a minimal, grey text item. Webpages with a total webpage size of <a href="https://www.machmetrics.com/speed-blog/website-size-the-average-web-page-size-is-more-than-2mb-twice-the-size-of-the-average-page-just-3-years-ago/">1MB or greater</a> would be styled in a way to warn the user of a large download requirement. (Light red background to attract immediate attention)</p>

        <h2>Scanning results</h2>
        <p>Having the ability to know the weight of a given listing is great, but what about privacy? Including an option to tell the user which listings will track, send your data to 3rd-party services, or display obnoxious ads should be more important than your data consumption. This can also be implemented with a simple UI:</p>
        <div class="img">
            <img src="https://tdarb.org/etc/better-search-results/scan-results.png" alt="Scan Search Results">
        </div>
        <div class="img">
            <img style="border-color:#F5A623;" src="https://tdarb.org/etc/better-search-results/search-item-scan.png" alt="Search Item Scanned">
        </div>
        <h3 style="color:#002CFC;">&bull; Ads Indicator</h3>
        <p>This tag element is added to listings that contain certain flagged advertisement scripts (Google adsense, carbon ads, buysellads, etc.). Advertisements don't necessarily mean a <i>bad</i> experience, so their styling is designed as more informational.</p>
        <h3 style="color: #FF4C06;">&bull; 3rd-Party Scripts Indicator</h3>
        <p>The tracking tag element is designed in a way to indicate a warning to the user. These scripts tend to ignore user privacy, so they have more importance to the user's security. These would include (but not be limited to) Google Analytics, Kissmetrics, Mixpanel, Chartbeat, etc. This element might also be a good way to "upsell" the user on using script blocking extensions like Ghostery or uBlock.</p>

        <h2>Companies reliant on advertisements</h2>
        <p>Won't companies dependent on embedded advertisements and tracking scripts push-back against this design concept? Probably.</p>
        <p>But too bad. My message to companies who rely on beefy ads and follower their users wherever they go, as a means to make a profit: <strong>Adapt or die.</strong></p>
        <p>Ruining your users' experience for the sake of monetizing your business is bad for said business in the long run. Personally, I believe it's border-line unethical. Your users never consent to these 3rd-party tracking scripts or give permission for you to force-download megabytes of data.</p> 
        <p>Stop feeding your customers garbage and you won't have to worry about more detailed search results. You'll be one of the <strong>good guys</strong> &#128077;. Figure out a way to make money without screwing over your users. It's just lazy to say "there is no other way" before even trying something else.</p>
    </main>
    <footer>
        <div class="container">
            <h2>Closing remarks</h2>
            <p>Search results should be optimized for the users. By including page size, advertisement warnings and tracking script indicators, you allow users to make better informed decisions about which sites they decide to visit.</p>
            <p>Companies would be forced to move away from shady practices and slim down their overall websites/applications in order to remain competitive.</p>
            <p>Currently, a great deal of websites place profit over their users' goals and experience - and that <b>needs to change</b>. Small UX improvements to search engine results, like the ones laid out in this article, can help <i>force</i> websites towards more transparency.</p>
        </div>
    </footer>
</body>
</html>